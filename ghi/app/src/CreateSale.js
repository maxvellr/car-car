import React, { useEffect, useState } from 'react';
import { Navigate } from 'react-router-dom';


function CreateSaleForm () {
    const [vins, setVins] = useState([]);
    const [salespeople, setSalespeople] = useState([]);
    const [customers, setCustomers] = useState([]);
    const [price, setPrice] = useState('');
    const [vin, setVin] = useState('');
    const [salesperson, setSalesperson] = useState('');
    const [customer, setCustomer] = useState('');
    const [submitted, setSubmitted] = useState(false);

    const handlePriceChange = (event) => {
        const value = event.target.value;
        setPrice(value);
    }

    const handleVinChange = (event) => {
        const value = event.target.value;
        setVin(value)
    }

    const handleSalespersonChange = (event) => {
        const value = event.target.value;
        setSalesperson(value)
    }

    const handleCustomerChange = (event) => {
        const value = event.target.value;
        setCustomer(value)
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {}


        data.price = price;
        data.automobile = vin;
        data.salesperson = salesperson;
        data.customer = customer;

        const saleUrl = 'http://localhost:8090/api/sales/'
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const autoUrl = `http://localhost:8100/api/automobiles/${vin}/`
        const autoConfig = {
            method: "put",
            body: JSON.stringify({"sold" : true}),
            headers: {
                'Content-Type': 'application/json',
            },
        }


        const [saleResponse, autoResponse] = await Promise.all([
            fetch(saleUrl, fetchConfig),
            fetch(autoUrl, autoConfig)
        ])
        if (saleResponse.ok && autoResponse.ok) {

            setPrice('');
            setVin('');
            setSalesperson('');
            setCustomer('');
            setSubmitted(true);

        }

    }

const fetchData = async () => {
    const autoUrl = 'http://localhost:8100/api/automobiles/';
    const salespersonUrl = 'http://localhost:8090/api/salespeople/';
    const customerUrl = 'http://localhost:8090/api/customers/';
    const autoResponse = await fetch(autoUrl);
    const salesResponse = await fetch(salespersonUrl);
    const customerResponse = await fetch(customerUrl);

    if (autoResponse.ok) {
        const data = await autoResponse.json();
        setVins(data.autos.filter(
            auto => auto.sold === false
        ))
    }
    if (salesResponse.ok) {
        const data = await salesResponse.json();
        setSalespeople(data.salespeople)
    }
    if (customerResponse.ok) {
        const data = await customerResponse.json();
        setCustomers(data.customers)
    }

}

useEffect(() => {
    fetchData();
}, []);

const filteredVins = vins.filter(vin => {
    return vin.sold === false;
});


    return(
        <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Create Sale</h1>
                        <form onSubmit={handleSubmit} id="create-shoes-form">
                        <div className="form-floating mb-3">
                            <input onChange={handlePriceChange} placeholder="Price" value={price} required type="text" autoComplete="off" name="price" id="price" className="form-control"/>
                            <label htmlFor="price">Price</label>
                        </div>
                        <div className="mb-3">
                        <select onChange={handleVinChange} value={vin} required name="vin" id="vin" className="form-select">
                                <option value="">Select Vehicle VIN</option>
                            {filteredVins.map(vin => {
                                return (
                                    <option key={vin.vin} value={vin.vin}>
                                        {vin.vin}
                                    </option>
                                );
                            })}
                        </select>
                        </div>
                        <div className="mb-3">
                        <select onChange={handleSalespersonChange} value={salesperson} required name="salesperson" id="id" className="form-select">
                                <option value="">Select a Salesperson</option>
                            {salespeople.map(salesperson => {
                                return (
                                    <option key={salesperson.id} value={salesperson.id}>
                                        {salesperson.first_name} {salesperson.last_name}
                                    </option>
                                );
                            })}
                        </select>
                        </div>
                        <div className="mb-3">
                        <select onChange={handleCustomerChange} value={customer} required name="customer" id="customer" className="form-select">
                                <option value="">Select a Customer</option>
                            {customers.map(customer => {
                                return (
                                    <option key={customer.first_name} value={customer.id}>
                                        {customer.first_name} {customer.last_name}
                                    </option>
                                );
                            })}
                        </select>
                        </div>
                        <button className="btn btn-success">Create</button>

                        </form>
                    </div>
                </div>
                {submitted && <Navigate to="/sales" />}
            </div>
    )
}
export default CreateSaleForm;
