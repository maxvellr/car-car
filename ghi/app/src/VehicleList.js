import React, { useEffect, useState } from "react";

function VehicleList() {
    const [models, setModels] = useState([]);

    const getModelData = async () => {
        const modelResponse = await fetch('http://localhost:8100/api/models/');

        if (modelResponse.ok) {
            const modelData = await modelResponse.json();
            setModels(modelData.models)
        }
    };

    useEffect(() => {
        getModelData()
    }, [])

    return (
        <table className="table align-middle table-cell-padding-y">
        <thead>
            <tr>
                <th>Make</th>
                <th>Model</th>
                <th>Photo</th>
            </tr>
        </thead>
        <tbody>
            {models.map(model => {
                return (
                    <tr key={ model.id }>
                        <td>{ model.manufacturer.name }</td>
                        <td>{ model.name }</td>
                        <td><img src={model.picture_url} alt={model.name}width="300px" height="200px"/></td>
                    </tr>
                );
            })}
        </tbody>
    </table>
    );
}

export default VehicleList
