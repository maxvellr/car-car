import { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';

function SalespeopleList() {
    const [salespeople, setSalespeople] = useState([])

    const getData = async () => {
        const response = await fetch('http://localhost:8090/api/salespeople/');

        if (response.ok) {
            const data = await response.json();
            setSalespeople(data.salespeople)
        }
    };

    useEffect(() => {
        getData()
    }, [])

    return (
    <div className="container px-4 py-4 tex-center">
        <h1>Salespeople</h1>
        <div className="d-grid gap-2 d-sm-flex justify-content-md-end">
            <Link to="/salespeople/new" className="btn btn-success btn-sm px-4 gap-3">+ Salesperson</Link>
        </div>
            <table className="table table-hover align-middle">
            <thead>
                <tr>
                <th>Employee ID</th>
                <th>First Name</th>
                <th>Last Name</th>
                </tr>
            </thead>
            <tbody>
                {salespeople.map(salesperson => {
                return (
                    <tr key={salesperson.id} >
                    <td>{ salesperson.employee_id }</td>
                    <td>{ salesperson.first_name }</td>
                    <td>{ salesperson.last_name }</td>
                    </tr>
                );
                })}
            </tbody>
            </table>
        </div>
    )

};

export default SalespeopleList;
