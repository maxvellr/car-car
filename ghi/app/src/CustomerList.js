import { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';

function CustomerList() {
    const [customers, setCustomer] = useState([])

    const getData = async () => {
        const response = await fetch('http://localhost:8090/api/customers/');

        if (response.ok) {
            const data = await response.json();
            setCustomer(data.customers)
        }
    };

    useEffect(() => {
        getData()
    }, [])

    return (
    <div className="container px-4 py-4 tex-center">
        <h1>Customers</h1>
        <div className="d-grid gap-2 d-sm-flex justify-content-md-end">
            <Link to="/customers/new" className="btn btn-success btn-sm px-4 gap-3">+ Customer</Link>
        </div>
            <table className="table table-hover align-middle">
            <thead>
                <tr>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Phone Number</th>
                <th>Address</th>
                </tr>
            </thead>
            <tbody>
                {customers.map(customer => {
                return (
                    <tr key={customer.id} >
                    <td>{ customer.first_name }</td>
                    <td>{ customer.last_name }</td>
                    <td>{ customer.phone_number }</td>
                    <td>{ customer.address }</td>
                    </tr>
                );
                })}
            </tbody>
            </table>
        </div>
    )

};

export default CustomerList;
