import { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';

function SalesList() {
    const [sales, setSales] = useState([])

    const getData = async () => {
        const response = await fetch('http://localhost:8090/api/sales/');

        if (response.ok) {
            const data = await response.json();
            setSales(data.sales)
        }
    };

    useEffect(() => {
        getData()
    }, [])

    return (
    <div className="container px-4 py-4 tex-center">
        <h1>Sales</h1>
        <div className="d-grid gap-2 d-sm-flex justify-content-md-end">
            <Link to="/sales/new" className="btn btn-success btn-sm px-4 gap-3">+ Sale</Link>
        </div>
            <table className="table table-hover align-middle">
            <thead>
                <tr>
                <th>Salesperson ID</th>
                <th>Salesperson</th>
                <th>Customer</th>
                <th>VIN</th>
                <th>Sale Price</th>
                </tr>
            </thead>
            <tbody>
                {sales.map(sale => {
                return (
                    <tr key={sale.id} >
                    <td>{ sale.salesperson_id }</td>
                    <td>{ sale.salesperson.first_name } { sale.salesperson.last_name }</td>
                    <td>{ sale.customer.first_name } { sale.customer.last_name }</td>
                    <td>{ sale.automobile }</td>
                    <td>{ sale.price }</td>
                    </tr>
                );
                })}
            </tbody>
            </table>
        </div>
    )

};

export default SalesList;
