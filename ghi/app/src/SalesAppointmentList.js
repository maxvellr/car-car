import React, { useEffect, useState } from 'react';

function FilterSalesAppointmentList() {
    const [salespeople, setSalespeople] = useState([]);
    const [salesperson, setSalesperson] = useState('');
    const [appointments, setSalesAppointments] = useState([]);

    const handleSalespersonChange = (event) => {
        const value = event.target.value;
        setSalesperson(value);
    }

    const handleDelete = async (id) => {
        const response = await fetch(`http://localhost:8090/api/sales/appointments/${id}` ,{
            method:'DELETE',
        })
        if(response.ok){
            console.log({"deleted": true})
            setSalesAppointments(appointments.filter(appointment => appointment.id !== id));
        }
    }

    const getData = async () => {
        const response = await fetch('http://localhost:8090/api/salespeople/');
        const appointmentResponse = await fetch('http://localhost:8090/api/sales/appointments');

        if (appointmentResponse.ok) {
            const appointmentData = await appointmentResponse.json();

            setSalesAppointments(appointmentData.sales_appointments);
        }

        if (response.ok) {
            const data = await response.json();
            setSalespeople(data.salespeople);
        }
    };

    useEffect(() => {
        getData();
    }, []);

    if (!appointments) {
        return <div>Loading...</div>; // or any other loading indicator
    }

    const filteredSalesAppointments = appointments.filter(appointment => {
        return !salesperson || appointment.salesperson_id === salesperson;
    });

    return (
        <div className="container px-4 py-4 text-center">
            <h1>Sales Appointments</h1>
            <div className="mb-3">
                <select onChange={handleSalespersonChange} value={salesperson} required name="salesperson" id="salesperson" className="form-select">
                    <option value="">Select a Salesperson</option>
                    {salespeople.map(salesperson => {
                        return (
                            <option key={salesperson.id} value={salesperson.employee_id}>
                                {salesperson.first_name} {salesperson.last_name}
                            </option>
                        );
                    })}
                </select>
            </div>
            <table className="table table-sm align-middle">
                <thead>
                    <tr>
                        <th>Salesperson ID</th>
                        <th>Salesperson</th>
                        <th>Date</th>
                        <th>Time</th>
                        <th>Customer</th>
                        <th>Phone Number</th>
                        <th>Reason</th>
                        <th>Delete</th>
                    </tr>
                </thead>
                <tbody>
                    {filteredSalesAppointments.map(appointment => {

                    const dateTime = new Date(appointment.date_time);
                    const date = dateTime.toLocaleDateString("en-US");
                    const time = dateTime.toLocaleTimeString("en-US").replace(":00 ", " ");
                    appointment.date = date;
                    appointment.time = time;

                        return (
                            <tr key={appointment.id}>
                                <td>{appointment.salesperson_id}</td>
                                <td>{appointment.salesperson.first_name} {appointment.salesperson.last_name}</td>
                                <td>{appointment.date}</td>
                                <td>{appointment.time}</td>
                                <td>{appointment.customer}</td>
                                <td>{appointment.phone_number}</td>
                                <td>{appointment.reason}</td>
                                <td>
                                <button type="button" className="btn btn-sm btn-outline-danger" onClick={() => handleDelete(appointment.id)}>Delete</button>
                                </td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    );
}

export default FilterSalesAppointmentList;
