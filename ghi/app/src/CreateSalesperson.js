import React, { useState } from 'react';
import { Navigate } from 'react-router-dom';

function CreateSalespersonForm () {
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [employeeId, setEmployeeId] = useState('');
    const [submitted,setSubmitted] = useState(false);

    const handleFirstNameChange = (event) => {
        const value = event.target.value;
        setFirstName(value);
    }

    const handleLastNameChange = (event) => {
        const value = event.target.value;
        setLastName(value);
    }

    const handleEmployeeIdChange = (event) => {
        const value = event.target.value;
        setEmployeeId(value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {}

        data.first_name = firstName;
        data.last_name = lastName;
        data.employee_id = employeeId;

        const salespersonUrl = 'http://localhost:8090/api/salespeople/'
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(salespersonUrl, fetchConfig);
        if (response.ok) {

            setFirstName('');
            setLastName('');
            setEmployeeId('');
            setSubmitted(true);

        }
    }

return(
    <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Add Salesperson</h1>
                    <form onSubmit={handleSubmit} id="create-salesperson-form">
                    <div className="form-floating mb-3">
                        <input onChange={handleFirstNameChange} placeholder="First name" value={firstName} required type="text" autoComplete="off" name="first_name" id="first_name" className="form-control"/>
                        <label htmlFor="first_name">First name</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handleLastNameChange} placeholder="Last name" value={lastName} required type="text" autoComplete="off" name="last_name" id="last_name" className="form-control"/>
                        <label htmlFor="last_name">Last name</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handleEmployeeIdChange} placeholder="Employee ID" value={employeeId} required type="text" autoComplete="off" name="employee_id" id="employee_id" className="form-control"/>
                        <label htmlFor="employee_id">Employee ID</label>
                    </div>
                    <button className="btn btn-success">Add +</button>
                    </form>
                    </div>
            </div>
            {submitted && <Navigate to="/salespeople"/>}
        </div>

);

}

export default CreateSalespersonForm;
