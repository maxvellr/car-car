import React, { useEffect, useState } from "react";
import { Navigate } from 'react-router-dom';

function SalesAppointmentForm() {
  const [salespeople, setSalespeople] = useState([]);
  const [submitted, setSubmitted] = useState(false);
  const [salesperson, setSalesperson] = useState("");
  const [reason, setReason] = useState("");
  const [time, setTime] = useState("");
  const [date, setDate] = useState("");
  const [customer, setCustomer] = useState("");
  const [phoneNumber, setPhoneNumber] = useState("");


  const handleCustomerChange = (event) => {
    setCustomer(event.target.value);
  }

  const handleDateChange = (event) => {
    setDate(event.target.value);
  }

  const handleTimeChange = (event) => {
    setTime(event.target.value);
  }

  const handleSalespersonChange = (event) => {
    setSalesperson(event.target.value);
  }

  const handleReasonChange = (event) => {
    setReason(event.target.value);
  }

  const handlePhoneNumberChange = (event) => {
    setPhoneNumber(event.target.value);
  }

  const handleSubmit = async (event) => {
    event.preventDefault();

    const data = {}

    data.customer = customer;
    data.phone_number = phoneNumber;
    data.salesperson = salesperson;
    data.reason = reason;
    data.date_time = new Date(date + " " + time);

    const url = 'http://localhost:8090/api/sales/appointments/'
    const fetchConfig = {
        method: "post",
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json',
        },
    }

    const response = await fetch(url, fetchConfig);
    if (response.ok) {
        setCustomer("");
        setPhoneNumber("");
        setSalespeople([]);
        setReason('');
        setDate('');
        setTime('');
        setSubmitted(true);
    }

  }

  const fetchData = async () => {
    const salespeopleUrl = 'http://localhost:8090/api/salespeople/';
    const salesResponse = await fetch(salespeopleUrl);
    if (salesResponse.ok) {
        const data = await salesResponse.json();
        console.log(data)
        setSalespeople(data.salespeople || []);
    }
  }



  useEffect(() => {
    fetchData();
  }, []);

  return (
    <div className="row">
      <div className="offset-2 col-8">
        <div className="shadow p-4 mt-4 forms">
          <h1>Create a Sales Appointment</h1>
          <form onSubmit={handleSubmit} id="create-sales-appointment-form">
            <div className="form-floating mb-3">
              <input value={customer} onChange={handleCustomerChange} required type="text"
                name="customer" id="customer" placeholder="Customer" className="form-control" />
              <label htmlFor="customer">Customer</label>
            </div>
            <div className="form-floating mb-3">
                <input value={phoneNumber} onChange={handlePhoneNumberChange} required type="text"
                name="phone_number" id="phone_number" placeholder="Phone Number" className="form-control"/>
                <label htmlFor="phone_number">Phone Number</label>
            </div>
            <div className="form-floating mb-3">
              <input value={date} onChange={handleDateChange} required type="date" name="date"
                id="date" placeholder="Date" className="form-control" />
              <label htmlFor="date">Date</label>
            </div>
            <div className="form-floating mb-3">
              <input value={time} onChange={handleTimeChange} required type="time" name="time"
                id="time" placeholder="Time" className="form-control" />
              <label htmlFor="time">Time</label>
            </div>
            <div className="mb-3">
              <select onChange={handleSalespersonChange} value={salesperson} required id="salesperson"
                name="salesperson" className="form-select">
                <option value="">Choose a Salesperson</option>
                {salespeople.map(salesperson => {
                  return (
                    <option key={salesperson.id} value={salesperson.id}>
                      {salesperson.first_name} {salesperson.last_name}
                    </option>
                  );
                })}
              </select>
            </div>
            <div className="form-floating mb-3">
              <input value={reason} onChange={handleReasonChange} required type="text"
                name="reason" id="reason" placeholder="Reason" className="form-control" />
              <label htmlFor="reason">Reason</label>
            </div>
            <button className="btn btn-success">Create</button>
          </form>
        </div>
      </div>
      {submitted && <Navigate to="/appointments"/>}
    </div>
  );
}

export default SalesAppointmentForm;
